package org.sun.online.service.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.sun.online.internal.common.annotation.EnableWebConfigure;

@SpringBootApplication(scanBasePackages = "org.sun.online.service.message")
@EnableWebConfigure
public class ServiceMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceMessageApplication.class, args);
    }

}
