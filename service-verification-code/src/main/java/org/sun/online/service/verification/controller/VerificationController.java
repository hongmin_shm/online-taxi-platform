package org.sun.online.service.verification.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.sun.online.internal.common.dto.verification.code.VerifyCodeRequest;
import org.sun.online.internal.common.response.Response;
import org.sun.online.internal.common.dto.verification.code.VerifyCodeResponse;
import org.sun.online.service.verification.application.VerificationApplicationService;
import org.sun.online.service.verification.types.Identity;
import org.sun.online.service.verification.types.PhoneNumber;

/**
 * 验证码API
 *
 * @author sunhongmin
 * @date 2022/5/28 23:53
 * @description
 */
@RestController
@RequestMapping("/verify-code")
@RequiredArgsConstructor
public class VerificationController {

    private final VerificationApplicationService verificationApplicationService;

    /**
     * 生成验证码
     *
     * @param identity    身份类型
     * @param phoneNumber 手机号码
     * @return
     */
    @GetMapping(value = "generate/{identity}/{phoneNumber}")
    public Response<VerifyCodeResponse> verifyCodeGenerator(@PathVariable("identity") String identity, @PathVariable("phoneNumber") String phoneNumber) {
        return Response.success(verificationApplicationService.verifyCodeGenerate(new Identity(identity), new PhoneNumber(phoneNumber)));
    }

    /**
     * 验证码校验
     *
     * @param verifyCodeRequest
     * @return
     */
    @PostMapping("/verify")
    public Response<Boolean> verify(@Validated({VerifyCodeRequest.VerifyCode.class}) VerifyCodeRequest verifyCodeRequest) {
        return Response.success(verificationApplicationService.verifyCode(verifyCodeRequest));
    }

}
