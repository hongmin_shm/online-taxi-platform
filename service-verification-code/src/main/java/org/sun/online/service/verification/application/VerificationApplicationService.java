package org.sun.online.service.verification.application;

import org.sun.online.internal.common.dto.verification.code.VerifyCodeRequest;
import org.sun.online.internal.common.dto.verification.code.VerifyCodeResponse;
import org.sun.online.service.verification.types.Identity;
import org.sun.online.service.verification.types.PhoneNumber;

/**
 * @author sunhongmin
 * @date 2022/5/28 23:54
 * @description
 * 为什么一定需要接口？eg: 美团cat要求必须使用接口（动态代理）
 */
public interface VerificationApplicationService {

    VerifyCodeResponse verifyCodeGenerate(Identity identity, PhoneNumber phoneNumber);

    boolean verifyCode(VerifyCodeRequest verifyCodeRequest);
}
