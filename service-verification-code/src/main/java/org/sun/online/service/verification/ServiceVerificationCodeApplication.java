package org.sun.online.service.verification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.sun.online.internal.common.annotation.EnableWebConfigure;

@SpringBootApplication(scanBasePackages = "org.sun.online.service.verification")
@EnableWebConfigure
public class ServiceVerificationCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceVerificationCodeApplication.class, args);
    }

}
