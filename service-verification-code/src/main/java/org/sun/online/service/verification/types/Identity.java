package org.sun.online.service.verification.types;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.sun.online.internal.common.common.exception.ServiceException;

/**
 * @author sunhongmin
 * @date 2022/6/4 18:52
 * @description
 */
public class Identity {

    @Getter
    private final String identity;

    public Identity(String identity) {
        if(StringUtils.isBlank(identity)){
            throw new ServiceException("identity is null");
        }
        this.identity = identity;
    }
}
