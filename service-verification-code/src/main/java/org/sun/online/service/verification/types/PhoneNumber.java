package org.sun.online.service.verification.types;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.sun.online.internal.common.common.exception.ServiceException;

/**
 * @author sunhongmin
 * @date 2022/6/4 18:23
 * @description
 */
public class PhoneNumber {
    @Getter
    private final String phoneNumber;

    public PhoneNumber(String phoneNumber) {
        if(StringUtils.isBlank(phoneNumber)){
            throw new ServiceException("phoneNumber is null");
        }
        this.phoneNumber = phoneNumber;
    }


}
