package org.sun.online.service.verification.application.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.sun.online.internal.common.common.exception.ServiceException;
import org.sun.online.internal.common.enums.IdentityEnum;
import org.sun.online.service.verification.ServiceVerificationCodeApplication;
import org.sun.online.service.verification.application.VerificationApplicationService;
import org.sun.online.service.verification.types.Identity;
import org.sun.online.service.verification.types.PhoneNumber;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceVerificationCodeApplication.class)
//@ActiveProfiles("local")
public class VerificationApplicationServiceImplTest {
    @Autowired
    private VerificationApplicationService verificationApplicationService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void verifyCodeGenerate() {
        List<String> result = new ArrayList<>(100);
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            result.add(verificationApplicationService.verifyCodeGenerate(new Identity("123"), new PhoneNumber("123")).getCode());
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);

        int size = result.size();
        Set<String> set = new HashSet<>(result);
        // 无重复
        Assert.assertEquals(size, set.size());
        // 长度均为6
        long count = result.stream().filter(it -> it.length() != 6).count();
        Assert.assertEquals(count, 0);
//        result.forEach(System.out::println);
    }

    @Test
    public void testEnum() {
        System.out.println(IdentityEnum.CUSTOM.name());
        System.out.println(IdentityEnum.CUSTOM.name().toLowerCase());


        Assert.assertThrows(ServiceException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                try {
                    IdentityEnum.checkIdentityLegal("aaa");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    throw e;
                }
            }
        });

        IdentityEnum.checkIdentityLegal("custom");
    }

    @Test
    public void testRandomNum(){
        for (int i = 0; i < 100; i++) {
            String code = String.valueOf((int)((Math.random() * 9 + 1) * Math.pow(10,5)));
            System.out.println(code);
        }
    }

}