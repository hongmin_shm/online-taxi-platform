```java

// bound系列可以事先绑定key值，opsFor系列使用时，每次都需要指定key

redisTemplate.opsForValue();//操作字符串
redisTemplate.opsForHash();//操作hash
redisTemplate.opsForList();//操作list
redisTemplate.opsForSet();//操作set
redisTemplate.opsForZSet();//操作有序set


redisTemplate.boundValueOps(key);
redisTemplate.boundHashOps(key);
redisTemplate.boundListOps(key);
redisTemplate.boundSetOps(key);
redisTemplate.boundZSetOps(key);

//所有的键值对都可以通过delete删除
redisTemplate.delete("hashKey");
```