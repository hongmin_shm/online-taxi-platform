## 1.8之前的老年代与1.8之后的元空间有什么区别？
老年代和元空间都是对JVM规范"方法区"的具体实现
在JDK1.7及之前的版本中，只有hotspot才有Perm区（老年代）
在JDK1.8里，将Perm区中的字符串常量移动至堆区，而类元信息、字段、静态属性、方法、常量等移动至元数据区，Perm区消失
元空间与老年代一个最重要的区别是，**老年代使用的是jvm堆内存，而元空间使用本地内存**

老年代存在的问题
1. 在垃圾回收时存在诸多问题，并且FGC时会移动类元信息
2. 启动时需要指定大小，很难进行调优，例如在web应用中需要动态加载类元信息，会导致Perm区OOM，如果设置了老年代大小，部署到新机器上，如果遗忘掉还会导致故障重现，不熟悉应用的人排查苦不堪言

## OOM产生原因总结
1. 系统运行，产生大量占用内存过多的对象，超过JVM的内存限制 （常见于使用poi的导出服务）
2. GC回收速度赶不上内存消耗的速度 （循环产生对象添加到集合）
3. 存在内存泄漏，不能及时GC（创建网络链接不关闭，打开IO不关闭，不再使用的对象未断开引用关系，使用静态变量持有大对象引用）

## 会出现的区域
> 除了程序计数器，其他区域都有可能
1. 堆
2. 元空间
3. 虚拟机栈
4. 本地方法栈
5. 堆外内存
6. 直接内存*

## JVM挂了的可能性
1. Linux 系统保护机制：OOM Killer 内存紧张时，根据分值kill 对应进程 (命令：`sudo egrep -i -r 'Out Of' /var/log` )
2. OOM

## 内存快照
通过内存溢出栈信息，如果发现：
1. 构造方法 <init> 是构造方法的字节码格式，一定是堆OOM
2. 如果发现OOM位置是类加载器的方法，那一定是元空间OOM (ClassLoader.difineClass...)

## 建议
1. 启动参数加上 `XX:+HeapDumpOnOutOfMemoryError`  `-XX:HeapDumpPath=xxxx/xxx` 发生OOM时会生成内存快照信息到指定的路径
2. 堆内存过大会导致OOM时生成的dump文件也特别大
3. 主动dump出JVM内存，会引发STW，需要注意

## 常见的OOM异常
1. java.lang.OutOfMemoryError: heap space
    + 使用量/数据量激增
    + 内存泄漏
2. java.lang.OutOfMemoryError: GC overhead limit exceeded (应用内存耗尽，GC无法腾出足够的空间，98%的总时间进行GC，并且在GC后仅回收不到2%的堆)
3. java.lang.OutOfMemoryError: PermGen space (永久代 1.8之后移除)
4. java.lang.OutOfMemoryError: Requested array size exceeded VM limit (请求的数组大小超过VM限制)
5. Exception in thread “main” java.lang.OutOfMemoryError: unable to create new native thread 没有足够的内存空间给线程分配java栈，基本上还是线程池代码写的有问题，比如说忘记shutdown


