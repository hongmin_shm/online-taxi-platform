## ThreadLocal
多线程环境中，共享变量的并发修改常常导致线程同步问题，ThreadLocal可以存储线程私有的本地变量，从而使线程之间的变量相互隔离  
因为ThreadLocal在线程执行的上下文可以传递变量的特性，所以可以很好的解决变量值传递问题：
常见的web项目中，session需要在执行过程中进行传递，此时我们有两种选择  
1. 每一个需要调用的方法，都增加session的入参
2. 使用ThreadLocal
很明显，方法中增加session的入参增加了代码的侵入性，并且无法避免传递过程中被篡改从而引发难以察觉的BUG，所以，当一个参数需要贯穿始终，那么使用ThreadLocal是很好的实现方式

## ThreadLocal使用示例
```java
public class CorpAccessUserHolder {

    private final static ThreadLocal<CorpUserInfo> accessUser = new ThreadLocal<>();

    // 设置授权用户信息
    public static void setAccessUser(CorpUserInfo corpAccessUser) {
        accessUser.set(corpAccessUser);
    }

    // 获取授权用户信息
    public static CorpUserInfo getAccountUser() {
        CorpUserInfo result = accessUser.get();
        if (Objects.isNull(result)) {
            throw new RuntimeException("登录状态过期，请重新获取accessToken");
        }
        return result;
    }

    // 清空上下文
    public static void clear() {
        accessUser.remove();
    }
}
```
典型用法是在web拦截器中对请求进行授权验证，如果验证通过时调用`CorpAccessUserHolder.setAccessUser`放入上下文中，这样在调用返回之前的任意时刻，都可以获得授权用户信息，从而省去了繁琐的传递  
ThreadLocal 使用时需要特别注意内存泄漏问题，这里不再赘述，只要记得在调用返回之前调用remove方法即可避免内存泄漏

## 父子线程传递问题
ThreadLocal是线程上下文，如果主线程开启子线程，还可以顺利获得本地变量吗？答案是否定的，以下是实验过程
```java
 public class TTLTest{
    public static void main(String[] args) {
        ThreadLocal<String> threadLocal = new ThreadLocal<>();
        InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();

        threadLocal.set("threadLocal-value");
        inheritableThreadLocal.set("inheritableThreadLocal-value");

        // 验证父子线程传递
        new Thread(() -> {
            System.out.println(threadLocal.get()); // null
            System.out.println(inheritableThreadLocal.get()); //inheritableThreadLocal-value
        }).start();

        threadLocal.remove();
        inheritableThreadLocal.remove();
    }
}      
```
可以看到，开启的子线程是无法获得父线程的本地变量的，所以java引入了`InheritableThreadLocal`,在子线程初始化时，会将父线程的本地变量传递到子线程  

## 线程池环境的本地变量传递问题
在大多数实际项目中，为了节省线程开启关闭的开销，常常使用线程池来提高线程的复用  
线程复用的同时，对本地变量的传递带来了新的影响，上文提到`InheritableThreadLocal`实现父子线程变量传递是在子线程初始化过程中，而池化的线程是不会重新初始化的，所以`InheritableThreadLocal`不能在线程池开辟的子线程
中传递，或者说，只会在子线程初始化时传递一次，而后在线程池中未被销毁之前，无法再次接受父线程的变量传递  
```java
public class TTLTest{
    public static void main(String[] args) {
        InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();
        // 验证线程池传递
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 10; i++) {
            inheritableThreadLocal.set("inheritableThreadLocal-value->" + i);
            executorService.submit(() -> {
                System.out.println(inheritableThreadLocal.get());
            });
        }
        executorService.shutdown();
        inheritableThreadLocal.remove();
    }
}
```
执行结果（部分）
```log
inheritableThreadLocal-value->0
inheritableThreadLocal-value->0
inheritableThreadLocal-value->0
inheritableThreadLocal-value->0
...
```
可以看出子线程只保留了最初获得的本地变量    
解决方案是阿里巴巴开源框架TTL: transmittable-thread-local

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>transmittable-thread-local</artifactId>
</dependency>
```

每次提交任务时，都会将当前的主线程的TTL数据copy到子线程里边，执行完成后再清除掉，同时子线程里边的修改不会对主线程生效，这样保证每次任务执行时互不干涉  
```java
public class TTLTest {
    public static void main(String[] args) {
        InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();
        TransmittableThreadLocal<String> transmittableThreadLocal = new TransmittableThreadLocal<>();
        // 验证线程池传递2
        ExecutorService executorService2 = TtlExecutors.getTtlExecutorService(Executors.newFixedThreadPool(1));
        for (int i = 0; i < 10; i++) {
            inheritableThreadLocal.set("inheritableThreadLocal-value->" + i);
            transmittableThreadLocal.set("transmittableThreadLocal-value->" + i);
            executorService2.submit(() -> {
                System.out.println(inheritableThreadLocal.get());
                System.out.println(transmittableThreadLocal.get());
            });
        }
        executorService.shutdown();
        inheritableThreadLocal.remove();
        transmittableThreadLocal.remove();
    }
}
```
执行结果（部分）  
```log
inheritableThreadLocal-value->0
transmittableThreadLocal-value->1
inheritableThreadLocal-value->0
transmittableThreadLocal-value->2
inheritableThreadLocal-value->0
transmittableThreadLocal-value->3
inheritableThreadLocal-value->0
transmittableThreadLocal-value->4
...
```
注意这句代码`TtlExecutors.getTtlExecutorService` 通过对线程池进行包装，从而实现TTL所具备的功能  

使用阿里开源的TransmittableThreadLocal 优雅的实现父子线程的数据传递，应用场景很多，企业中应用也比较广泛  

原理参考: [微服务中使用阿里开源的TTL，优雅的实现身份信息的线程间复用](https://www.51cto.com/article/710590.html)