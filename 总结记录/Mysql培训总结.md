## 架构组件
mysql主要分为Server以及Engine  

server  
客户端  
    连接器-管理连接，权限验证  
    查询缓存-命中则直接返回结果  
    分析器-词法分析、语法分析  
    优化器-执行计划生成，索引选择 （explain)  
    执行器-操作引擎，返回结果  

engine  
    存储引擎-存储数据，提供读写接口  

??长连接的弊端：占用系统资源，导致内存占用过大

## 优化器*
基本参数 `optimizer_prune_level` 默认为启发式，根据条件跳过部分计划  
参数 `optimizer_search_depth` 默认递归深度62，可理解为join的表的个数阶乘。rds=4  

## InnoDB
执行流程*  
binlog  
redo log  
undo log 

innodb三大特性  
insert buffer 针对非唯一的二级索引，提高性能  
double write 防止页断裂，DB数据页最小单位为16k,但原子写4k需要4次  
adaptive hash index 加速二级索引->聚集索引的速度  

数据页，内存与磁盘交互的最小单元，默认16k，常见保存用户数据得到是数据页，blob页  

pages -> extent 1M -> segment -> tablespace   

**数据页内保存着上个，下一个页号组成双向链表（b+树叶子节点也是双向链表）页内记录是按主键形成单向链表 （这也是为什么主键要求递增）**  

主键顺序是逻辑顺序  
页内数据怎么找（二分法  

InnoDB数据页结构  
FileHeader PageHeader infirmum+supermum userRecords FreeSpace pageDirectory FileTrailer  

隐藏列：rowid或主键 事物ID 回滚指针  

字符编码 utf-mb4 问题*  
为什么不建议select *  
为什么不建议列可为null  
text如何保存  
delete 后，数据所占空间并不会释放，需要进行碎片整理  
`OPTIMIZE [LOCAL | NO_WRITE_TO_BINLOG] TABLE table_name1 [, table_name2] ...`

## 索引
按组织方式分:  
主键索引:在数据⻚上 ，对主键 进行 B+树算法后，保存 主键值 与⻚号 2列，格式 与普通记录一样，用记录头里的record_type=1区分。  
二级索引:对相关列(多个列按顺序进行排序)再加上主键值 创建 B+树。叶子节 点不存放数据，需要用主键再查一遍数据(回表)，最少查找2棵树 (索引覆盖除 外)。  
> 回表:索引也可理解为只有少几个列的排序好的小表，索引内没这个数据，就 需要去原来的大表中拿这个数据。  

按使用方式分:  
单列索引:1个字段  
联合(覆盖、多列)索引:多个字段  
cardinality基数，区分度越高越合适放前面( ??? crop_id_业务属性id 索引合理 性)  
前缀索引:1个或多个字段组成，常⻅字符串字符过⻓，取前N位使用 唯一索引:1个或多个字段组成，有唯一性约束  
全文索引:基本不用  
自适应索引:系统自行处理，无法干扰  
??? 唯一索引坏处 (死锁问题)  

## 索引失效
隐式转换
```sql
select 'id' > 1; # 主键类型异常
select date_format(create_at, '%m') = 9; # 查月份大小
# uft8 转 utf8mb4
```
统计信息失真*  
排序(归并排序、队列排序)、回表因素*  

索引选择&定位  
索引选择:本质上是 CBO cost base optimizer，成本代价的计算与选择。流程如下  
1. 根据where条件，找出有可能用到的索引  
2. 计算全表扫描代价  
3. 计算不同索引的代价;及优化后的代价(如排序、多索引合并等)  
4. join一个表计算一次，取最小值为当前步骤代价，与下一个表再join计算  
5. 对比不同，取最小值;相同则取第1个索引(相似索引的坑)  
 
索引是否越多越好 (旧版本提倡5个以内，新版本或支持10左右没有问题，待查阅确认*)  

## 索引定位:B+树的搜索
1. 打开表定位到root page，根节点一般常驻内存。索引根路径也有缓存在系统表中  
2. 根据B+树搜索，从根节点的索引开始 seek 到 叶子节点  

## 统计信息
优化器依赖统计信息来计算代价。默认持久化存储，默认采样20个⻚数据由全局参 数 innodb_stats_persistent_sample_pages 控制，也可以表级处理。  
具体的信息保存在 mysql.innodb_table_stats 与 mysql.innodb_index_stats 二个 表中。  
（案例：建立索引后发现执行计划中索引失效，通过调整大的innodb_stats_persistent_sample_pages采样值来试图精确统计信息，最终达到了优化目的）  
+ 自动更新  
  + 新增索引  
  + 表第一次open: on_metadata 一般不实用关闭  
  + 1/16 左右数据变更  
+ 异常处理  
  + 新增索引，对全部索引重新采样  
  + 加大采样数 ALTER TABLE ttt STATS_SAMPLE_PAGES=80;  
  + optimize table （也可以手动修改stats表，不建议） 整理碎片时，填充因子 默认100，但它不是最紧凑的!  

mysql.tables rows 准确性（不准确，因为这个数值是基于统计信息的，myisam引擎的数值是准确的，计数器）  
统计信息异常的后果*  

## explain
> 可以追加 explain format=json 来显示更多的信息，更容易理解  

type列  
ref:返回不唯一的等值; eq_ref:关联主键或唯一记录 range:索引范围扫描 index:利用索引进行的全记录扫描  
all:全表扫描  
extra:额外的描述，用了一些优化点，比如ICP MRR BKA等  
ICP index condition pushdown 索引中包含where条件时，在引擎层直接过滤  
MRR multi range read 二级索引->主键索引时，用rowid_buffer 进行id sort排序 再查询  
BKA batch key access join的key放在buffer 结合mrr 批量读主键  

`SELECT @@optimizer_switch; ## 查看优化点是否开启`  

??? 全表扫描是否最差 (权衡代价，全表有可能不是最坏的情况：索引回表也需要代价)  

key & key_len 列:
选择最终使用的索引，及用组合索引中使用前几个字段。(对组合索引优化很有用)  

possible_keys列
对于每个可能使用的索引，都会调用相关函数来计算预估rows与cost，相似索引越多，越容易出错。

## 延伸
**1个sql语句，查找出最优或局部最优的执行路径，快速返回结果。每个执行路 径都有算子与代价，代价越小，执行越快。优化本质就是扫描更少的⻚，用小 表(小结果集)驱动大表。**

### 代价
cost = server cost + engine cost  

server层:主要是cpu代价，处理计算每一条数据(一行一行处理的)，默认0.2  
engine层:主要是io代价，指从内存或磁盘读1个数据⻚ ，默认1;临时表默认40  
> select * from mysql.server_cost ; select * from mysql.engine_cost;

cost计算依赖统计信息，比如 stat_clustered_index_size 主键信息， key_info.key_length 、 ref_length索引键分布，rows行扫描数等等，计算十分 复杂。  

### 索引访问方式 ref&range
ref 
```sql
select * from ttt where field1=1;  
```
range 
```sql
select * from ttt where field1>1 or field2 in (1,2,3); # 产生区间
select * from ttt where field1>1 or field2=2; # 优化器自行选择
```
??? 能否用多个索引 (是可以应用到多个索引的，但同时说明，索引建立的有问题)

## OPTIMIZE TABLE
在Mysql中，如果我们使用delete删除了表中大量数据，或者我们对含有 可变长度文本类型VARCHAR、TEXT、BLOB的表进行了很多更改，被删除的数据记录仍然保持在Mysql的连接清淡中，因此数据存储文件的大小并不会随着数据删除而减小  

这些数据仍然占用空间并且还会导致执行Sql操作耗费更多性能和时间，因此对Mysql进行碎片整理十分必要  

命令完整格式: `OPTIMIZE [LOCAL | NO_WRITE_TO_BINLOG] TABLE table_name1 [, table_name2] ...`
OPTIMIZE TABLE可以一次性对多个表进行碎片整理，只需要在OPTIMIZE TABLE后面接多个表名，并以英文逗号隔开即可。  
例如：`OPTIMIZE TABLE order_info_0,order_info_1`

备注：
1.MySQL官方建议不要经常(每小时或每天)进行碎片整理，一般根据实际情况，只需要每周或者每月整理一次即可。  
2.OPTIMIZE TABLE只对MyISAM，BDB和InnoDB表起作用，尤其是MyISAM表的作用最为明显。此外，并不是所有表都需要进行碎片整理，一般只需要对包含上述可变长度的文本数据类型的表进行整理即可。  
3.在OPTIMIZE TABLE运行过程中，MySQL会锁定表。  
4.默认情况下，直接对InnoDB引擎的数据表使用OPTIMIZE TABLE，可能会显示「 Table does not support optimize, doing recreate + analyze instead」的提示信息。这个时候，我们可以用mysqld --skip-new或者mysqld --safe-mode命令来重启MySQL，以便于让其他引擎支持OPTIMIZE TABLE。  

针对 InnoDB 引擎
使用命令 `alter table table_name engine = innodb;` 重建表的存储引擎，重组索引的数据和存储

总结一下：清理表的碎片可以提高 MySQL 性能，在日常工作中我们可以定期执行表碎片整理，从而提高 MySQL 性能。


= 快速定位
in 每个值都搜索  
between > 先快速定位再范围 limit server层处理  
group by 创建临时表  
order by 可能创建临时表 子查询优化 in 的优化 临时表写法  

## mysql 三种日志
Buffer Pool是MySQL进程管理的一块内存空间，有减少磁盘IO次数的作用。
redo log是InnoDB存储引擎的一种日志，主要作用是崩溃恢复，有三种刷盘策略，有innodb_flush_log_at_trx_commit 参数控制，推荐设置成2。
undo log是InnoDB存储引擎的一种日志，主要作用是回滚。
binlog是MySQL Server层的一种日志，主要作用是归档。同时Mysql主从同步也是依赖binlog，读库通过监听写库binlog日志文件来实现数据同步
MySQL挂了有两种情况：操作系统挂了MySQL进程跟着挂了；操作系统没挂，但是MySQL进程挂了。

通过`SHOW ENGINE INNODB STATUS`;来查看死锁日志
`select @@tx_isolation;` 查看事物隔离级别
`set autocommit=0;` 关闭自动提交
`select @@autocommit`
https://zhuanlan.zhihu.com/p/245556254

加锁机制：乐观锁、悲观锁
锁粒度：表锁、页锁、行锁
兼容性：共享锁、排他锁
锁模式：记录锁、gap锁、next-key锁、意向锁、插入意向锁
InnoDB实现了标准的行级锁，包括共享锁s、排他锁x
共享锁：允许持锁事物读取一行
排他锁：允许持锁事物更新或者删除一行


