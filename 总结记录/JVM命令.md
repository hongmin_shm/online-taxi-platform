`jps -lvm` 查看当前机器运行的java进程，或者 ps -efww | grep xxx
`jstack -l pid` 查看线程堆栈信息

## jstack 分析CPU占用率高问题
> 线程数高导致频繁上下文切换、频繁GC等,CPU占用率过高常常导致应用无响应
1. `top`命令查看占用CPU最高的java进程
2. `top -H -p pid` 查看进程内，占用CPU过高的线程 pid
3. `printf '%x\n' pid ` 将线程pid转换为16进制 nid
4. `jstack pid | grep '0xnid' -C5 --color '` 查看线程状态，并通过nid来过滤出上一步得到的占用CPU最高的线程栈信息
5. `jstat -gc pid 1000` 1000表示采样间隔（ms)
> S0C/S1C、S0U/S1U、EC/EU、OC/OU、MC/MU分别代表两个Survivor区、Eden区、老年代、元数据区的容量和使用量
> YGC/YGT、FGC/FGCT、GCT则代表YoungGc、FullGc的耗时和次数以及总耗时

6. `jstack pid | grep 'Thread.State' | sort -nr | uniq -c` 可以使用此命令对线程总体运行状态有一个总体把握

## 上下文切换
1. `vmstat 1` 1表示采样间隔，cs列代表上下文切换次数
2. `pidstat -w 1` 可以查看进程的监控，cswch和nvcswch表示自愿及非自愿切换

## 磁盘
1. `df -hl`来查看文件系统状态
2. 磁盘问题还是性能上的问题。我们可以通过`iostat -d -k -x`来进行分析，最后一列%util可以看到每块磁盘写入的程度，而rrqpm/s以及wrqm/s分别表示读写速度，一般就能帮助定位到具体哪块磁盘出现问题了

## 内存
见[OOM总结](/总结记录/OOM总结.md)
## 堆外内存
> 堆外内存溢出表现就是物理常驻内存增长快，报错的话视使用方式都不确定，如果由于使用Netty导致的，那错误日志里可能会出现OutOfDirectMemoryError错误，如果直接是DirectByteBuffer，那会报OutOfMemoryError: Direct buffer memory  
> 堆外内存溢出往往是和NIO的使用相关  
> 我们可以跟踪一下DirectByteBuffer对象的内存情况，通过jmap -histo:live pid手动触发fullGC来看看堆外内存有没有被回收。如果被回收了，那么大概率是堆外内存本身分配的太小了，通过-XX:MaxDirectMemorySize进行调整。如果没有什么变化，那就要使用jmap去分析那些不能被gc的对象，以及和DirectByteBuffer之间的引用关系了

## gc问题和线程
> gc 除了影响CPU之外，也会影响内存，一般通过jstat查看GC指标。
> 线程数过多而且gc不及时也会引发OOM,大部分为 java.lang.OutOfMemoryError: unable to create new native thread 没有足够的内存空间给线程分配java栈
> 除了分析dump文件分析内存，还可以使用`pstreee -p pid |wc -l` 查看总体线程数
> 或者直接通过 `ls -l /proc/pid/task | wc -l` 即为进程内线程数量

## GC专题
> 处理使用`jstat -gc`查看gc信息，更过时候也可以通过gc日志分析问题，启动参数中加上`-verbose:gc` `-XX:+PrintGCDetails` `-XX:+PrintGCDateStamps` `-XX:+PrintGCTimeStamps`来开启 GC 日志。  
针对gc日志，我们大致能根据垃圾回收频率以及时长，从而对症下药

youngGC 过频繁

youngGC 频繁一般是短周期小对象较多，先考虑是不是 Eden 区/新生代设置的太小了，看能否通过调整-Xmn、-XX:SurvivorRatio 等参数设置来解决问题。如果参数正常，但是 young gc 频率还是太高，就需要使用 Jmap 和 MAT 对 dump 文件进行进一步排查了。

youngGC 耗时过长

耗时过长问题就要看 GC 日志里耗时耗在哪一块了。以 G1 日志为例，可以关注 Root Scanning、Object Copy、Ref Proc 等阶段。Ref Proc 耗时长，就要注意引用相关的对象。Root Scanning 耗时长，就要注意线程数、跨代引用。Object Copy 则需要关注对象生存周期。而且耗时分析它需要横向比较，就是和其他项目或者正常时间段的耗时比较。比如说图中的 Root Scanning 和正常时间段比增长较多，那就是起的线程太多了。
![gclog](project-static/G1垃圾回收日志.png)

触发 fullGC

G1 中更多的还是 mixedGC，但 mixedGC 可以和 youngGC 思路一样去排查。触发 fullGC 了一般都会有问题，G1 会退化使用 Serial 收集器来完成垃圾的清理工作，暂停时长达到秒级别，可以说是半跪了。

fullGC 的原因可能包括以下这些，以及参数调整方面的一些思路：

并发阶段失败：在并发标记阶段，MixGC 之前老年代就被填满了，那么这时候 G1 就会放弃标记周期。这种情况，可能就需要增加堆大小，或者调整并发标记线程数-XX:ConcGCThreads。
晋升失败：在 GC 的时候没有足够的内存供存活/晋升对象使用，所以触发了 Full GC。这时候可以通过-XX:G1ReservePercent来增加预留内存百分比，减少-XX:InitiatingHeapOccupancyPercent来提前启动标记，-XX:ConcGCThreads来增加标记线程数也是可以的。
大对象分配失败：大对象找不到合适的 region 空间进行分配，就会进行 fullGC，这种情况下可以增大内存或者增大-XX:G1HeapRegionSize。
程序主动执行 System.gc()：不要随便写就对了。
另外，我们可以在启动参数中配置-XX:HeapDumpPath=/xxx/dump.hprof来 dump fullGC 相关的文件，并通过 jinfo 来进行 gc 前后的 dump

> jinfo -flag +HeapDumpBeforeFullGC pidjinfo -flag +HeapDumpAfterFullGC pidjinfo -flag +HeapDumpBeforeFullGC pidjinfo -flag +HeapDumpAfterFullGC pid

这样得到 2 份 dump 文件，对比后主要关注被 gc 掉的问题对象来定位问题。

## 网络
netstat