AST 抽象语法树
client
server
    连接器
    分析器
    优化器（explain）
        RBO
        CBO
    执行器
存储引擎

5.7
`set profiling=1`   
`show profiles`  
`show profile`
`show profile for query QUERY_ID`

5.7之后的版本推荐使用
performance schema
`show databases`
`show tables`
`use performance_schema`  

`show variables like 'performance_schema`  

`set performance_schema=ON` 无效，必须通过配置文件修改重启  

`show processlist`  
[DRUID数据库连接池-为监控而生的数据库连接池](https://github.com/alibaba/druid/)

数据库版本和默认的存储引擎
mysql> select @@version,@@default_storage_engine;
查看事务是否自动提交
mysql> select @@global.autocommit;

## 数据类型优化
1. 数据类型在满足需求前提下，长度越小越好
2. 使用简单的数据类型
    + 整型比数字类型操作代价更低
    + 使用mysql自建类型存储日期和时间，而不是使用字符串
    + 使用整形存储ip地址(需要权衡，需要用到函数转换)
3. 避免null值，null值的列使得索引、索引统计和值比较都更加复杂，对mysql来说很难优化

### 整数类型
TINYINT - 8 位 (tinyint占用1字节,1字节占用8位，经过换算(2的8次方减1)就是255) 有符号 -127-128 无符号（unsigned） 0-255
SMALLINT - 16
MEDIUMINT - 24
INT - 32
BIGINT -64 


