## 线程数估算
线程数 = cpu可用核心数 / 1 - 阻塞系数
IO密集型 阻塞系数接近1
cpu密集型 阻塞系数接近0


renewal-percent-threshold
> Renews threshold 期望客户端续约次数 节点数*每分钟续约次数*renewal-percent-threshold
> Renews (last min) 最后一分钟续约次数 节点数*每个节点续约次数

guava LoadingCache 本地缓存

Eureka 在生产环境的优化

Eureka wiki
https://docs.spring.io/spring-cloud-netflix/docs/3.0.2/reference/html/#spring-cloud-eureka-server

## Eureka Server集群同步(待查阅修正)
```text
各种情况下的注册中心信息同步
1. 注册成功时，会同步服务节点信息到下一个注册中心节点(上报节点)
2. 服务续约，会将服务节点信息同步到所有注册中心集群上
3. 服务下线，同2
4. 服务剔除，不会同步注册中心集群，因为注册中心集群中的每个节点都会有剔除操作，所以不需要同步
```

## 自我保护机制
```text
自我保护机制的工作机制是：如果在15分钟内超过85%的客户端节点都没有正常的心跳，那么Eureka就认为客户端与注册中心出现了网络故障，Eureka Server自动进入自我保护机制，此时会出现以下几种情况：

1. Eureka Server不再从注册列表中移除因为长时间没收到心跳而应该过期的服务。
2. Eureka Server仍然能够接受新服务的注册和查询请求，但是不会被同步到其它节点上，保证当前节点依然可用。
3. 当网络稳定时，当前Eureka Server新的注册信息会被同步到其它节点中。
因此Eureka Server可以很好的应对因网络故障导致部分节点失联的情况，而不会像ZK那样如果有一半不可用的情况会导致整个集群不可用而变成瘫痪。
```

## 缓存机制
```text
Eureka Server存在三个变量：(registry、readWriteCacheMap、readOnlyCacheMap)保存服务注册信息，
默认情况下定时任务每30s将readWriteCacheMap同步至readOnlyCacheMap，每60s清理超过90s未续约的节点，
Eureka Client每30s从readOnlyCacheMap更新服务注册信息，而UI则从registry更新服务注册信息。
```

## 服务重启正确步骤
> 服务重启时，应该先停服，后操作下线，如果先下线，服务续约还会重新注册到注册中心

## eureka server启动后，dashboard 中集群节点出现在 unavailable-replicas 中的问题解决
1. 确保集群各注册中心节点 spring.application.name一致
2. 开启register-with-eureka 自注册与fetch-registry 拉取
3. defaultZone 链接需要配置主机名，不能使用ip （需要在主机配置 /etc/hosts 域名ip映射）
4. eureka.instance.name 各注册中心节点name不能相同
5. ip-address 不需要配置

***注册中心，注册失败后会尝试后边的地址，尝试次数为3 （从左向右一次尝试，成功即停止 消费者拉取同理***
https://www.cnblogs.com/chiangchou/p/eureka-3.html 《Eureka注册中心源码系列》 

## 重新编辑springboot starter
https://blog.csdn.net/BingTaiLi/article/details/109842838

## TransmittableThreadLocal
TransmittableThreadLocal（https://github.com/alibaba/transmittable-thread-local）是Alibaba开源的、用于解决“在使用线程池等会缓存线程的组件情况下传递ThreadLocal”问题的 InheritableThreadLocal 扩展。若希望 TransmittableThreadLocal 在线程池与主线程间传递，需配合TtlRunnable和TtlCallable使用。