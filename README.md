# 网约车

#### 介绍
网约车项目
>   该项目是一款标准网约车应用，符合我国交通部对网约车监管的技术要求，并且通过了交通部对网约车线上和线下能力认定。 项目中核心功能包括:账户系统，订单系统，支付系统，地图引擎， 派单引擎，消息系统等网约车核心解决方案。    
>   
>   技术架构完全采用微服务架构设计，应用成熟的接口安全设计方案， 将引用一套***领域驱动设计（DDD）规范落地实现***，使系统趋向于高度灵活的代码结构，采用分布式锁保证了分布式环境中的数据同步，用分布式事务解决了分布式环境中的数据一致性等。

---  
目标
+ 首要目标是***从零开始***实现一套完整的网约车落地项目，涵盖微服务应用的整套相关技术框架的应用
+ 除了微服务相关组件的实践应用以外，将会采用***领域驱动设计***实现代码开发，力争采用一套可落地的DDD实践来开发出高度灵活的系统技术架构
+ 系统开发过程中，将会把每一阶段的优化点提炼出来，进而***沉淀一套有价值的JAVA生态技术文章***，内容包括但不限于（DDD落地规范、Mysql调优实践、分布式事物、分布式锁...）
+ ***期待加入以及关注，如有疑问直接进行私信或留言半天之内会得到回复***...

---
总结性文章
1. [实践-仿照@EnableEurekaServer实现自动装配](总结记录/实践-仿照@EnableEurekaServer实现自动装配.md)
2. [JVM线上故障排查套路总结](总结记录/JVM命令.md)
3. [领域驱动设计实践规范](总结记录/领域驱动设计实践规范.md)
4. 

--- 
当前计划
1. ~~common建设：异常体系、工具包、基础Bean~~
2. ~~统一异常处理~~，~~traceId链路追踪，logback接入~~
3. ~~接口文档平台搭建~~ [smart-doc](https://smart-doc-group.github.io/#/zh-cn/)  
4. ~~手机号验证码接口~~
5. 乘客端服务-乘客端登录
6. openfeign api maven 模块拆分

#### 软件架构
架构图
![项目架构图](z-project-static/project-staticimage.png)
业务层
---
|模块|项目名|描述|
|---|---|---|
|乘客端|api-passenger|乘客端|
|司机端|api-driver|司机端|
|司机听单|api-listen-order|司机听单|

能力层
---
|模块|项目名|描述|
|---|---|---|
|app升级|service-app-update|-|
|订单|service-order|-|
|派单|service-order-dispatch|-|
|乘客用户管理|service-passenger-user|-|
|短信|service-sms|-|
|计价|service-valuation|-|
|验证码|service-verification-code|-|
|钱包|service-wallet|-|
|支付|service-payment|-|
|地图|service-map|-|

common 通用能力层
---
|模块|项目名|描述|
|---|---|---|
|通用、工具类、异常、校验等|internal-common|-|

SpringCloud组件
|组件名|描述|
|---|---|
|zuul|微服务网关，对外公网入口，限流、权限校验等|
|eureka|服务注册中心|
|hystrix|熔断器，具有请求失败重试，服务降级等功能，保障服务可用|

#### 安装教程 （当前为调试阶段，尚只能进行接口调试，界面设计尚未开始进行）

1. fork 项目，clone到本地
2. 执行`mvn install -DskipTests`,接口文档会自动生成到./src/main/resources/static目录
3. 启动模块，例如启动service-verification-code模块
4. 访问 http://localhost:8002/index.html 即可查看接口文档，进行调试
![img.png](z-project-static/debug示例.png)
5. 


#### 技术选型及版本
1. JDK 11
2. spring-boot-starter-parent 2.6.7
3. zuul
4. boot-admin
5. git config server
6. zipkin sleuth
7. 

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 服务测算
> 20 个服务 部署5个节点 client=100  
> 默认心跳30s一次，一分钟两次  
> 1分钟1个server 发送心跳两百次  
> 一天 288000 访问量