package org.sun.online.internal.common.enums;

import org.sun.online.internal.common.common.exception.ServiceException;
import org.sun.online.internal.common.constant.RedisKeyPrefixConstant;

import java.util.Objects;

/**
 * @author sunhongmin
 * @date 2022/6/4 18:50
 * @description
 */
public enum IdentityEnum {
    /**
     * 乘客
     */
    CUSTOM,
    /**
     * 司机
     */
    DRIVER;

    public static IdentityEnum getInstance(String name) {
        for (IdentityEnum instance : IdentityEnum.values()) {
            if (instance.name().toLowerCase().equals(name)) {
                return instance;
            }
        }
        throw new ServiceException(String.format("根据名称无法获得身份信息，name:%s", name));
    }

    public String getRedisKeyPrefix() {
        String name = this.name().toLowerCase();
        switch (name) {
            case "custom":
                return RedisKeyPrefixConstant.PASSENGER_LOGIN_CODE_KEY_PRE;
            case "driver":
                return RedisKeyPrefixConstant.DRIVER_LOGIN_CODE_KEY_PRE;
            default:
                throw new ServiceException(String.format("未对应的映射关系，name:%s", this.name()));
        }

    }

    public static void checkIdentityLegal(String identityName) {
        for (IdentityEnum value : IdentityEnum.values()) {
            if (Objects.equals(value.name().toLowerCase(), identityName)) {
                return;
            }
        }

        throw new ServiceException(String.format("不合法的身份类型名称 identityName:%s", identityName));
    }

}
