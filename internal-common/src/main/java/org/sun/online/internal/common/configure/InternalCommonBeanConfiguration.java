package org.sun.online.internal.common.configure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.sun.online.internal.common.advice.GlobalExceptionInterceptor;
import org.sun.online.internal.common.trace.TransIdFilter;
import org.sun.online.internal.common.web.config.WebConfig;
import org.sun.online.internal.common.web.config.WebMvcConfig;

/**
 * @author sunhongmin
 * @date 2022/5/30 13:27
 * @description
 */
@Configuration
@Import({WebConfig.class, WebMvcConfig.class})
@ConditionalOnBean(WebMarkerConfiguration.Marked.class)
public class InternalCommonBeanConfiguration {

    @Bean
    public GlobalExceptionInterceptor createGlobalExceptionInterceptor(){
        // 全局异常处理
        return new GlobalExceptionInterceptor();
    }

    @Bean
    public TransIdFilter createTransIdFilter(){
        // TraceId 过滤器
        return new TransIdFilter();
    }

}
