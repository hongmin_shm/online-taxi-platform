package org.sun.online.internal.common.dto.verification.code;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sunhongmin
 * @date 2022/6/4 16:22
 * @description
 */
@Data
@AllArgsConstructor
public class VerifyCodeResponse implements Serializable {

    /**
     * 验证码
     */
    private String code;
}
