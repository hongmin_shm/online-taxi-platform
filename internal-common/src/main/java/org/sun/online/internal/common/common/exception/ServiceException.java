package org.sun.online.internal.common.common.exception;

import org.sun.online.internal.common.common.exception.code.ResponseCodeEnum;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description
 */
public class ServiceException extends OnlineTaxiBeseException{

    @Override
    public ResponseCodeEnum getCode() {
        return ResponseCodeEnum.CODE_INTERNAL_ERROR;
    }

    public ServiceException() {}

    public ServiceException(String message) {
        super(message);
    }

}
