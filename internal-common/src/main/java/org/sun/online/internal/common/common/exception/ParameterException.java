package org.sun.online.internal.common.common.exception;

import org.sun.online.internal.common.common.exception.code.ResponseCodeEnum;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description
 */
public class ParameterException extends OnlineTaxiBeseException{

    @Override
    public ResponseCodeEnum getCode() {
        return ResponseCodeEnum.CODE_PARAM_ERROR;
    }

    public ParameterException() {}

    public ParameterException(String message) {
        super(message);
    }

}
