package org.sun.online.internal.common.annotation;

import org.springframework.context.annotation.Import;
import org.sun.online.internal.common.configure.WebMarkerConfiguration;

import java.lang.annotation.*;

/**
 * @author sunhongmin
 * @date 2022/6/4 11:38
 * @description 开启Web通用处理器：全局异常，TraceId，fastJson消息转换器 等，有效减少配置导入重复代码
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(WebMarkerConfiguration.class)
public @interface EnableWebConfigure {

}
