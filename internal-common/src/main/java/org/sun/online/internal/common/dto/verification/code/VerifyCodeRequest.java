package org.sun.online.internal.common.dto.verification.code;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @author sunhongmin
 * @date 2022/5/29 20:07
 * @description
 */
@Data
public class VerifyCodeRequest implements Serializable {

    public interface VerifyCode {
    }

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空", groups = {VerifyCode.class})
    private String phoneNumber;

    /**
     * 司乘信息
     */
    @NotBlank(message = "司乘信息不能为空", groups = VerifyCode.class)
    private String identity;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空", groups = VerifyCode.class)
    private String code;

}
