package org.sun.online.internal.common.common.exception;

import org.sun.online.internal.common.common.exception.code.ResponseCodeEnum;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description
 */
public class AuthException extends OnlineTaxiBeseException{
    @Override
    public ResponseCodeEnum getCode() {
        return ResponseCodeEnum.AUTH_ERROR;
    }

    public AuthException() {}

    public AuthException(String message) {
        super(message);
    }

}