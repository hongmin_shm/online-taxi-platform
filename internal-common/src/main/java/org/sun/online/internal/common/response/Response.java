package org.sun.online.internal.common.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.sun.online.internal.common.common.exception.code.ResponseCodeEnum;
import org.sun.online.internal.common.trace.TraceIdUtils;

/**
 * @author sunhongmin
 * @date 2022/5/29 19:53
 * @description 统一响应包装
 */
@Getter@Setter
@Accessors(chain = true)
public final class Response<T> {

    /**
     * 链路追踪ID
     */
    private String traceId;

    /**
     * 响应码
     * @mock 100
     */
    private Integer code = ResponseCodeEnum.CODE_SUCCESS.getCode();

    /**
     * 响应信息
     * @mock 操作成功
     */
    private String message;

    /**
     * 返回数据
     * @mock success
     */
    private T data;

    public Response(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.traceId = TraceIdUtils.getTraceId();
    }

    public static Response<?> success() {
        return new Response<>(ResponseCodeEnum.CODE_SUCCESS.getCode(), "操作成功", "");
    }

    public static <T> Response<T> success(T data) {
        return new Response<>(ResponseCodeEnum.CODE_SUCCESS.getCode(), "操作成功", data);
    }

    public static <T> Response<T> fail(Integer code, String message, T data) {
        return new Response<>(code, message, data);
    }

    public static Response<?> fail() {
        return new Response<>(ResponseCodeEnum.CODE_INTERNAL_ERROR.getCode(), "系统内部异常", null);
    }
}
