package org.sun.online.internal.common.common.exception;

import org.sun.online.internal.common.common.exception.code.ResponseCodeEnum;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description 异常基类
 */
public abstract class OnlineTaxiBeseException extends RuntimeException {

    protected int code;

    public abstract ResponseCodeEnum getCode();

    public OnlineTaxiBeseException() {
        super();
    }

    public OnlineTaxiBeseException(String message, Throwable cause) {
        super(message, cause);
    }

    public OnlineTaxiBeseException(String message) {
        super(message);
    }

    public OnlineTaxiBeseException(Throwable cause) {
        super(cause);
    }

    public OnlineTaxiBeseException(int code) {
        super();
        this.code = code;
    }

    public OnlineTaxiBeseException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public OnlineTaxiBeseException(int code, String message) {
        super(message);
        this.code = code;
    }

    public OnlineTaxiBeseException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }
}
