package org.sun.online.internal.common.common.exception.code;

import lombok.Getter;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description 异常基类
 */
@Getter
public enum ResponseCodeEnum {
    /**
     * 1xx  表示成功   （成功无需解释）
     * 2xx  表示参数异常 （调用后台数据时参数验证失败等）
     * 3xx  程序运行时异常 （没有捕获的异常，如空指针异常等）
     * 4xx  程序内部错误  （预知已经捕获的异常，如获取缓存异常）
     * 5xx  内部服务调用失败  （调用第三方服务出错，如调用dubbo超时等）
     * 6xx  第三方服务调用失败  （淘宝接口调用失败，如超时，session失效，授权失效等）
     * 7xx  权限错误 （会话失效、无权限）
     * 8xx  程序挂了  （没有想好)
     */
    // 1xx  表示成功   （成功无需解释）
    CODE_SUCCESS(100, "调用成功（1xx统一表示调用成功）"),
    // 2xx  表示参数异常 （调用后台数据时参数验证失败等）
    CODE_PARAM_ERROR(200, "请求参数异常（2xx统一表示参数异常）"),
    CODE_ENTITY_CONVERT_ERROR(201, "对象转换异常"),
    CODE_REQUEST_MODEL_ERROR(202, "接口请求参数类型不匹配异常"),
    // 3xx  程序运行时异常 （没有捕获的异常，如空指针异常等）
    CODE_RUNTIME_ERROR(300, "服务运行时异常（3xx统一表示程序运行时异常）"),
    RESULT_NULL(302, "未查询到数据"),
    RESULT_304(304, "数据重复"),
    CODE_SERVICE_UNKOWN_ERROR(311, "服务未知异常"),
    CODE_SERVICE_EDIT_RPC_ERROR(312, "编辑器的dubbo服务异常"),
    CODE_SERVICE_WEB_PARAMS_ERROR(313, "前端传入参数异常"),
    CODE_SERVICE_DB_SELECT_ERROR(314, "数据库查询异常"),
    CODE_SERVICE_DB_INSERT_ERROR(315, "数据库新增异常"),
    CODE_SERVICE_DB_UPDATE_ERROR(316, "数据库修改异常"),
    CODE_SERVICE_DB_DELETE_ERROR(317, "数据库删除异常"),
    CODE_SERVICE_XML_ANALYSIS_ERROR(322, "请求参数异常"),
    // 4xx  程序内部错误  （预知已经捕获的异常，如获取缓存异常）
    CODE_INTERNAL_ERROR(400, "系统内部异常（4xx统一表示程序内部错误）"),
    // 5xx  内部服务调用失败  （调用第三方服务出错，如调用dubbo超时等）
    CODE_INTERNAL_RPC_ERROR(500, "远程调用异常（5xx统一表示内部服务调用失败）"),
    // 6xx  第三方服务调用失败  （淘宝接口调用失败，如超时，session失效，授权失效等）
    CODE_TAOBAO_API_ERROR(600, "请求淘宝api异常（6xx统一表示第三方服务调用失败）"),
    CODE_API_INVOKE_ERROR(601, "API接口调用已知错误"),
    // 7xx  权限错误 （会话失效、无权限）
    CODE_AUTHORITY(700, "token 过期（7xx统一表示权限错误）"),
    CODE_SESSION_KEY_ERROR(701, "session 过期"),
    CODE_NO_USER_ERROR(702, "session 获取用户失败"),
    AUTH_ERROR(703, "认证异常"),
    SHORT_OUATH_ERROR(704, "短授权异常"),
    USER_ROLE_ERROR(705, "权限受限"),
    CODE_LIMITED_AUTHORITY(706, "用户权限不够"),
    CODE_SHUTDOWN_MAINTENANCE(707, "停机维护"),
    CODE_DING_LIMITED_AUTHORITY(708, "用户不在钉钉授权范围内"),
    CODE_NEED_UPGRADE(709, "当前版本需要升级"),
    EXPORT_CODE(750, "异步导出统一返回码"),
    // 8xx  程序挂了
    CODE_JVM_DOWN(800, "jvm down（8xx统一表示程序异常）");

    private Integer code;
    private String message;

    ResponseCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
