package org.sun.online.internal.common.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author sunhongmin
 * @date 2022/6/4 11:42
 * @description
 */
@Configuration
public class WebMarkerConfiguration {

    @Bean
    public Marked createMarkedInstance(){
        return new Marked();
    }

    class Marked{
    }
}
