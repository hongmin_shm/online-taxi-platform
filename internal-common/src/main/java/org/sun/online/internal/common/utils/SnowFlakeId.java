package org.sun.online.internal.common.utils;

import org.sun.online.internal.common.common.exception.ServiceException;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description snowflake 雪花算法
 */
public class SnowFlakeId {
    /**
     * 起始的时间戳
     */
    private final static long START_STAMP = 1480166465631L;
    /**
     * 默认数据中心
     */
    private final static long DEFAULT_DATA_CENTER_ID = 31;
    /**
     * 默认机器id
     */
    private final static long DEFAULT_MACHINE_ID = 31;
    /**
     * 序列号占用的位数
     */
    private final static long SEQUENCE_BIT = 12;
    /**
     * 机器标识占用的位数
     */
    private final static long MACHINE_BIT = 5;
    /**
     * 数据中心占用的位数
     */
    private final static long DATA_CENTER_BIT = 5;
    /**
     * 每一部分的最大值
     */
    private final static long MAX_SEQUENCE = -1L ^ (-1L << SEQUENCE_BIT);
    /**
     * 每一部分向左的位移
     */
    private final static long MACHINE_LEFT = SEQUENCE_BIT;
    private final static long DATA_CENTER_LEFT = SEQUENCE_BIT + MACHINE_BIT;
    private final static long TIMESTAMP_LEFT = DATA_CENTER_LEFT + DATA_CENTER_BIT;
    //数据中心
    private static long dataCenterId = DEFAULT_DATA_CENTER_ID;
    //机器标识
    private static long machineId = DEFAULT_MACHINE_ID;
    //序列号
    private static long sequence = 0L;
    //上一次时间戳
    private static long lastStamp = -1L;

    private SnowFlakeId() {
        dataCenterId = DEFAULT_DATA_CENTER_ID;
        machineId = DEFAULT_MACHINE_ID;
    }

    /**
     * 产生下一个ID
     *
     * @return
     */
    public synchronized static String nextId() {
        long currStmp = getNewStamp();
        if (currStmp < lastStamp) {
            throw new ServiceException("Clock moved backwards.  Refusing to generate id");
        }
        if (currStmp == lastStamp) {
            //相同毫秒内，序列号自增
            sequence = (sequence + 1) & MAX_SEQUENCE;
            //同一毫秒的序列数已经达到最大
            if (sequence == 0L) {
                currStmp = getNextMill();
            }
        } else {
            //不同毫秒内，序列号置为0
            sequence = 0L;
        }
        lastStamp = currStmp;
        long value = (currStmp - START_STAMP) << TIMESTAMP_LEFT //时间戳部分
                | dataCenterId << DATA_CENTER_LEFT       //数据中心部分
                | machineId << MACHINE_LEFT             //机器标识部分
                | sequence;                             //序列号部分
        return String.valueOf(value);
    }

    private static long getNextMill() {
        long mill = getNewStamp();
        while (mill <= lastStamp) {
            mill = getNewStamp();
        }
        return mill;
    }

    private static long getNewStamp() {
        return System.currentTimeMillis();
    }
}