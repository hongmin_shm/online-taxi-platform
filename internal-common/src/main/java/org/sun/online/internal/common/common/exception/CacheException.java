package org.sun.online.internal.common.common.exception;

import org.sun.online.internal.common.common.exception.code.ResponseCodeEnum;

/**
 * @author sunhongmin
 * @date 2022/5/26 20:39
 * @description
 */
public class CacheException extends OnlineTaxiBeseException {
    @Override
    public ResponseCodeEnum getCode() {
        return ResponseCodeEnum.CODE_INTERNAL_ERROR;
    }

    public CacheException() {
    }

    public CacheException(String s) {
        super(s);
    }

    public CacheException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheException(Throwable cause) {
        super(cause);
    }
}
